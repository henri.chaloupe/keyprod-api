export interface Product {
    title: string;
    key: string;
    weight: number;
}
