import { IsInt, IsString } from 'class-validator';

export class CreateProductDto {
  @IsString()
  readonly title: string;
  @IsString()
  readonly key: string;
  @IsInt()
  readonly weight: number;
}