import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { OrdersModule } from './orders.module';
import { OrdersService } from './orders.service';

describe('Orders', () => {
  let app: INestApplication;
  let ordersService: OrdersService = new OrdersService();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [OrdersModule],
    })
      .overrideProvider(OrdersService)
      .useValue(ordersService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET orders`, () => {
    return request(app.getHttpServer())
      .get('/orders')
      .expect(200)
      .expect({
        data: ordersService.findAll(),
      });
  });

  afterAll(async () => {
    await app.close();
  });
});