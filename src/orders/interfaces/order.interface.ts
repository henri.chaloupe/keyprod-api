export interface Order {
    title: string;
    products?: OrderProduct[];
}

interface OrderProduct {
    key: string;
    quantity: number;
}