import { Injectable } from '@nestjs/common';
import { Product } from './interfaces/product.interface';
import * as productsData from '../../data/products.json';

@Injectable()
export class ProductsService {
  private readonly products: Product[] = productsData;

  create(order: Product) {
    this.products.push(order);
  }

  findAll(): Product[] {
    return this.products;
  }

  findOne(id): Product {
    return this.products[id];
  }

  findOneByKey(key): Product {
    return this.products.find(p => p.key.toLocaleLowerCase() === key.toLocaleLowerCase());
  }

  delete(key) {
    this.products.splice(key, 1);
  }
}