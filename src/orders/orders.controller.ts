import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from './interfaces/order.interface';
import { OrdersService } from './orders.service';


@Controller('orders')
export class OrdersController {
    constructor(private readonly ordersService: OrdersService) {}
    
    @Post()
    async create(@Body() createOrderDto: CreateOrderDto) {
        this.ordersService.create(createOrderDto);
    }
    
    @Get()
    async findAll(): Promise<Order[]> {
        return this.ordersService.findAll();
    }
    
    @Get(':id')
    findOne(
        @Param('id', new ParseIntPipe())
        id: number,
    ) {
        return this.ordersService.findOne(id);
    }
    
    @Post(':id/product/:productKey')
    addProduct(
        @Param('id', new ParseIntPipe())
        id: number,
        @Param('productKey')
        productKey: string,
    ) {
        this.ordersService.addProduct(id, productKey);
    }
    
    @Delete(':id')
    async delete(
        @Param('id', new ParseIntPipe())
        id: number,
    ) {
        this.ordersService.delete(id);
    }
}