import { Injectable } from '@nestjs/common';
import { Order } from './interfaces/order.interface';
import * as OrdersData from '../../data/orders.json';

@Injectable()
export class OrdersService {
  private readonly orders: Order[] = OrdersData;

  create(order: Order) {
    this.orders.push({ ...order, products: [] });
  }

  findAll(): Order[] {
    return this.orders;
  }

  findOne(id): Order {
    return this.orders[id];
  }

  delete(key) {
    this.orders.splice(key, 1);
  }

  addProduct(id, productKey) {
    const product = this.findOne(id).products.find(p => p.key === productKey);
    if (product) {
        product.quantity += 1;
        return;
    }
    return this.findOne(id).products.push({ key: productKey, quantity: 1 });
  }
}