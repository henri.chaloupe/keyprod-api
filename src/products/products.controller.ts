import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './interfaces/product.interface';
import { ProductsService } from './products.service';


@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService) {}
    
    @Post()
    async create(@Body() createProductDto: CreateProductDto) {
        this.productsService.create(createProductDto);
    }
    
    @Get()
    async findAll(): Promise<Product[]> {
        return this.productsService.findAll();
    }
    
    @Get(':id')
    findOne(
        @Param('id', new ParseIntPipe())
        id: number,
    ) {
        return this.productsService.findOne(id);
    }
    
    @Get('key/:key')
    findOneByKey(
        @Param('key')
        key: string,
    ) {
        return this.productsService.findOneByKey(key);
    }
        
    @Delete(':id')
    async delete(
        @Param('id', new ParseIntPipe())
        id: number,
    ) {
        this.productsService.delete(id);
    }
}